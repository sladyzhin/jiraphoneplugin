package ut.com.ig.jira.plugin.clicktocall;

import org.junit.Test;
import com.ig.jira.plugin.clicktocall.api.MyPluginComponent;
import com.ig.jira.plugin.clicktocall.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}