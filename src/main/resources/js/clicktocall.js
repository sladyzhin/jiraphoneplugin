AJS.$(function () {
    JIRA.Dialogs.editPhone = new JIRA.FormDialog({
        id: "phone-dialog",
        trigger: "a.phoneaction-edit-phone",
        ajaxOptions: JIRA.Dialogs.getDefaultAjaxOptions,
        onSuccessfulSubmit : function() {
            var newPhone = AJS.$("#phone").val();
            if (newPhone) {
                AJS.$('#phone-field').text(newPhone);
            } else {
                AJS.$('#phone-field').text(AJS.I18n.getText("common.words.none"));
            }
            AJS.$('a.phoneaction-edit-phone').attr('href', function(i, a) {
                return a.replace(/(phone=)[0-9\\-]*/, '$1' + newPhone);
            });
        },
        onDialogFinished : function () {
            JIRA.Messages.showSuccessMsg(AJS.I18n.getText("com.ig.jira.plugin.clicktocall.user.profile.phone.edit.success"));
        },
        autoClose: true
    });
});

AJS.$(function () {
    JIRA.Dialogs.searchPhone = new JIRA.FormDialog({
        id: "search-phone-dialog",
        trigger: "a.search-phone",
        ajaxOptions: JIRA.Dialogs.getDefaultAjaxOptions
    });
});