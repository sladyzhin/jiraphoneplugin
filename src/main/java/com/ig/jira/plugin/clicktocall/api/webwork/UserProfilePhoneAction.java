package com.ig.jira.plugin.clicktocall.api.webwork;

import static com.ig.jira.plugin.clicktocall.constant.Constants.NAME_PARAM;
import static com.ig.jira.plugin.clicktocall.constant.Constants.PHONE_PARAM;

import com.ig.jira.plugin.clicktocall.api.service.UserService;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import webwork.action.ActionContext;

import java.util.Map;

public class UserProfilePhoneAction extends JiraWebActionSupport {
    private static final String PHONE_CONTEXT_PARAM = "Phone";
    private static final String NAME_CONTEXT_PARAM = "Name";
    private static final String ERROR_MESSAGE = "Phone must contain only numbers and hyphens";
    private static final String REGEX = "^[0-9\\-]*$";

    private UserService userService;
    private String phone;
    private String name;

    public UserProfilePhoneAction(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String doDefault() {
        setPhone(getHttpRequest().getParameter(PHONE_PARAM));
        setName(getHttpRequest().getParameter(NAME_PARAM));
        return INPUT;
    }

    protected void doValidation() {
        Map parameters = ActionContext.getParameters();
        setPhone(((String[]) parameters.get(PHONE_CONTEXT_PARAM))[0]);
        setName(((String[]) parameters.get(NAME_CONTEXT_PARAM))[0]);
        if (!this.phone.matches(REGEX)) {
            this.addErrorMessage(ERROR_MESSAGE);
        }
    }

    public String doExecute() {
        userService.addUserPhone(name, phone);
        return returnComplete();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
