package com.ig.jira.plugin.clicktocall.constant;

public final class Constants {
    public static final String PHONE_PARAM = "phone";
    public static final String NAME_PARAM = "name";
}
