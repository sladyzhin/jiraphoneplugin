package com.ig.jira.plugin.clicktocall.api.rest;

import static com.ig.jira.plugin.clicktocall.constant.Constants.NAME_PARAM;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.ig.jira.plugin.clicktocall.api.service.UserService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/users")
public class RestResource {
    private final UserService userService;

    public RestResource(UserService userService) {
        this.userService = userService;
    }

    @GET
    @AnonymousAllowed
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserPhone(@QueryParam(NAME_PARAM) String name) {
        return Response.ok(userService.searchPhone(name)).build();
    }
}