package com.ig.jira.plugin.clicktocall.api.service;

import com.ig.jira.plugin.clicktocall.api.model.ao.User;

import java.util.List;

public interface UserService {
    User getUserByPhone(String phone);

    void addUserPhone(String userName, String phone);

    String getUserPhone(String userName);

    List<User> searchPhone(String userName);
}
