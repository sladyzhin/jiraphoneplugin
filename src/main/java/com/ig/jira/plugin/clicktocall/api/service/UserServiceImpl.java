package com.ig.jira.plugin.clicktocall.api.service;

import static com.google.common.base.Preconditions.checkNotNull;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.user.UserManager;
import com.google.common.collect.ImmutableMap;
import com.ig.jira.plugin.clicktocall.api.model.ao.User;
import net.java.ao.Query;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Named
public class UserServiceImpl implements UserService {
    private static final String NAME_AO_QUERY_PARAM = "NAME = ?";
    private static final String DIGITS_REGEX = "\\D";
    private static final String QUERY_BY_PHONE_ERROR = "There shouldn't be more then 1 user with the same phone! ";
    private static final String QUERY_BY_NAME_ERROR = "There shouldn't be more then 1 user with the same name! ";

    private final ActiveObjects ao;
    private final UserManager userManager;

    @Inject
    public UserServiceImpl(@ComponentImport ActiveObjects ao, @ComponentImport UserManager userManager) {
        this.ao = checkNotNull(ao);
        this.userManager = checkNotNull(userManager);
    }

    @Override
    public User getUserByPhone(String phone) {
        List<User> userList = Arrays.stream(ao.find(User.class))
                .peek(user -> user.setPhone(user.getPhone().replaceAll(DIGITS_REGEX, "")))
                .filter(user -> user.getPhone().equals(phone)).collect(Collectors.toList());
        if (userList.size() == 0) {
            return null;
        } else if (userList.size() == 1) {
            return userList.get(0);
        } else {
            throw new IllegalStateException(QUERY_BY_PHONE_ERROR + phone);
        }
    }

    @Override
    public void addUserPhone(String userName, String phone) {
        User user = getOrCreateUser(ao, userName);
        user.setPhone(phone);
        user.save();
    }

    @Override
    public String getUserPhone(String userName) {
        if (StringUtils.isBlank(userName)) {
            userName = currentUserName();
        }
        User[] users = ao.find(User.class, Query.select().where(NAME_AO_QUERY_PARAM, userName));
        if (users.length == 0) {
            return null;
        } else if (users.length == 1) {
            return users[0].getPhone();
        } else {
            throw new IllegalStateException(QUERY_BY_NAME_ERROR + userName);
        }
    }

    @Override
    public List<User> searchPhone(String userName) {
        return Arrays.stream(
                ao.find(User.class, Query.select().where("NAME LIKE ?", "%" + userName + "%")))
                .collect(Collectors.toList());
    }

    private String currentUserName() {
        return Objects.requireNonNull(userManager.getRemoteUsername());
    }

    private User getOrCreateUser(ActiveObjects ao, String userName) {
        if (StringUtils.isBlank(userName)) {
            userName = currentUserName();
        }
        User[] users = ao.find(User.class, Query.select().where(NAME_AO_QUERY_PARAM, userName));
        if (users.length == 0) {
            return createUser(ao, userName);
        } else if (users.length == 1) {
            return users[0];
        } else {
            throw new IllegalStateException(QUERY_BY_NAME_ERROR + userName);
        }
    }

    private User createUser(ActiveObjects ao, String userName) {
        return ao.create(User.class, ImmutableMap.of("NAME", userName));
    }
}
