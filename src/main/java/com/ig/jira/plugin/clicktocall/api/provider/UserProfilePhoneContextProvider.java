package com.ig.jira.plugin.clicktocall.api.provider;

import static com.ig.jira.plugin.clicktocall.constant.Constants.NAME_PARAM;
import static com.ig.jira.plugin.clicktocall.constant.Constants.PHONE_PARAM;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.plugin.webfragment.contextproviders.AbstractJiraContextProvider;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.VelocityParamFactory;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.ig.jira.plugin.clicktocall.api.service.UserService;

import java.util.HashMap;
import java.util.Map;

public class UserProfilePhoneContextProvider extends AbstractJiraContextProvider {
    private static final String REQUEST_CONTEXT_PARAM = "requestContext";

    private final UserService userService;

    public UserProfilePhoneContextProvider(UserService userService) {
        this.userService = userService;
    }

    @Override
    //For some reason JiraHelper is null, so there is a workaround to access context parameters
    //https://jira.atlassian.com/browse/JRASERVER-65578
    public Map getContextMap(ApplicationUser applicationUser, JiraHelper jiraHelper) {
        JiraAuthenticationContext jiraAuthenticationContext = ComponentAccessor.getJiraAuthenticationContext();
        VelocityParamFactory velocityParamFactory = ComponentAccessor.getVelocityParamFactory();
        Map<String, Object> defaultVelocityParams = velocityParamFactory.getDefaultVelocityParams(jiraAuthenticationContext);
        VelocityRequestContext velocityRequestContext = (VelocityRequestContext) defaultVelocityParams.get(REQUEST_CONTEXT_PARAM);
        Map<String, Object> context = new HashMap<>();
        context.put(NAME_PARAM, velocityRequestContext.getRequestParameter(NAME_PARAM));
        context.put(PHONE_PARAM, userService.getUserPhone(velocityRequestContext.getRequestParameter(NAME_PARAM)));
        return context;
    }
}
