package com.ig.jira.plugin.clicktocall.api.servlet;

import static com.ig.jira.plugin.clicktocall.constant.Constants.PHONE_PARAM;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.ig.jira.plugin.clicktocall.api.model.ao.User;
import com.ig.jira.plugin.clicktocall.api.service.UserService;
import com.ig.jira.plugin.clicktocall.util.ServletUtils;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Scanned
public class IssuesServlet extends HttpServlet {
    private static final String ISSUE_NAVIGATOR_URL = "/secure/IssueNavigator.jspa?jql=";

    private final UserService userService;
    private final UserManager userManager;
    private final LoginUriProvider loginUriProvider;

    public IssuesServlet(UserService userService, @ComponentImport UserManager userManager,
                         @ComponentImport LoginUriProvider loginUriProvider) {
        this.userService = userService;
        this.userManager = userManager;
        this.loginUriProvider = loginUriProvider;
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
        String username = userManager.getRemoteUsername(req);
        if (username == null) {
            ServletUtils.redirectToLogin(loginUriProvider, req, res);
            return;
        }
        String jql = "NOT%20resolution=Unresolved";
        User user = userService.getUserByPhone(req.getParameter(PHONE_PARAM));
        if (user != null) {
            jql += "%20AND%20reporter=" + user.getName();
        }
        res.sendRedirect(req.getContextPath() + ISSUE_NAVIGATOR_URL + jql);
    }
}
