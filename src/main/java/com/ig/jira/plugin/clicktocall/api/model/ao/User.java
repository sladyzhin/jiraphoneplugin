package com.ig.jira.plugin.clicktocall.api.model.ao;

import net.java.ao.Entity;

import javax.xml.bind.annotation.XmlAttribute;

public interface User extends Entity {
    @XmlAttribute
    String getName();

    void setName(String name);

    @XmlAttribute
    String getPhone();

    void setPhone(String phone);
}
