package com.ig.jira.plugin.clicktocall.api.provider;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.webfragment.contextproviders.AbstractJiraContextProvider;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.ig.jira.plugin.clicktocall.api.service.UserService;

import java.util.Map;

public class PhoneBlockContextProvider extends AbstractJiraContextProvider {
    private static final String REPORTER_PHONE_PARAM = "reporterPhone";
    private static final String ASSIGNEE_PHONE_PARAM = "assigneePhone";
    private static final String ISSUE_PARAM = "issue";

    private final UserService userService;

    public PhoneBlockContextProvider(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Map getContextMap(ApplicationUser applicationUser, JiraHelper jiraHelper) {
        Map<String, Object> context = jiraHelper.getContextParams();
        Issue issue = (Issue) context.get(ISSUE_PARAM);
        ApplicationUser reporter = issue.getReporter();
        ApplicationUser assignee = issue.getAssignee();
        if (reporter != null) {
            context.put(REPORTER_PHONE_PARAM, userService.getUserPhone(reporter.getUsername()));
        }
        if (assignee != null) {
            context.put(ASSIGNEE_PHONE_PARAM, userService.getUserPhone(assignee.getUsername()));
        }
        return context;
    }
}
